package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	//Entity : Making the normal class as an Entity Class
	//Id     : Applying the Primary Key Contraint to the respective column
	//GeneratedValue: Auto_Increment
	//Column : providing our own name for the column
	
	@Id@GeneratedValue
	private int studentId;
	private String studentName;
	private String gender;
	private String course;
	private double fee;

   
   public Student(){
	   super();
   }
   public Student(int studentId,String gender,String course,String studentName,double fee){
	   super();
	   this.studentId = studentId;
	   this.studentName = studentName;
	   this.gender = gender;
	   this.course = course;
	   this.fee = fee;
   }
public int getStudentId() {
	return studentId;
}
public void setStudentId(int studentId) {
	this.studentId = studentId;
}
public String getStudentName() {
	return studentName;
}
public void setStudentName(String studentName) {
	this.studentName = studentName;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getCourse() {
	return course;
}
public void setCourse(String course) {
	this.course = course;
}
public double getFee() {
	return fee;
}
public void setFee(double fee) {
	this.fee = fee;
}
@Override
public String toString() {
	return "Student [studentId=" + studentId + ", studentName=" + studentName + ", gender=" + gender + ", course="
			+ course + ", fee=" + fee + "]";
}

}
